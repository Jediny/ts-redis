import { PathLike } from 'fs';

export function appendFile(
    _file: PathLike | number,
    _data: any,
    callback: (err: NodeJS.ErrnoException | null) => void
): void {
    callback(null);
}
