import { addCounter, saveCount, saveBody } from './process-request';

jest.mock('../libs/redis-source');
jest.mock('fs');

const bodyWithCount = { test: 'test', count: 5 };
const bodyWithoutCount = { test: 'test' };

describe('Tests for processing track events', () => {
    it('Increase counter', (cb) => {
        addCounter(5, (err, result) => {
            expect(err).toBeUndefined();
            expect(result).toBe(6);
            cb();
        });
    });
    it('Increase counter with negative number', (cb) => {
        addCounter(-5, (err, result) => {
            expect(err).toBeUndefined();
            expect(result).toBe(-4);
            cb();
        });
    });

    it('Save count with string count in body', (cb) => {
        saveCount({ count: 'test', ...bodyWithoutCount }, (err, result) => {
            expect(err).toBeDefined();
            expect(result).toBeUndefined();
            cb();
        });
    });
    it('Save count with count in body', (cb) => {
        saveCount(bodyWithCount, (err, result) => {
            expect(err).toBeUndefined();
            expect(result).toBe(6);
            cb();
        });
    });
    it('Save count without count in body', (cb) => {
        saveCount(bodyWithoutCount, (err, result) => {
            expect(err).toBeUndefined();
            expect(result).toBeUndefined();
            cb();
        });
    });

    it('Save body', (cb) => {
        saveBody(bodyWithoutCount, (err, result) => {
            expect(err).toBeUndefined();
            if (!result) {
                return cb(new Error('Save body result is empty'));
            }
            expect(result).toHaveProperty('ts');
            expect(result.body).toMatchObject(bodyWithoutCount);
            cb();
        });
    });
});
