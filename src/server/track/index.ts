import { Request, Response } from 'express';
import processRequest from './process-request';

/**
 * @description Function process track request
 * @export
 * @param {Request} req
 * @param {Response} res
 */
export default function postTrack(req: Request, res: Response) {
    processRequest(req.body, (err) => {
        if (err) {
            res.status(500).json({ err: err.message });
        } else {
            res.json({ save: true });
        }
    });
}
