import * as async from 'async';
import * as fs from 'fs';
import { incrementKey } from '../libs/redis-source';

const COUNT = 'count';

/**
 * @description Function process track request
 * @export
 * @param {{ [key: string]: any }} body
 * @param {((err: Error | undefined) => void)} cb
 */
export default function processRequest(
    body: { [key: string]: any },
    cb: (err: Error | undefined) => void
): void {
    async.parallel(
        [
            (callback) => saveCount(body, callback),
            (callback) => saveBody(body, callback)
        ],
        (err) => {
            cb(err || undefined);
        }
    );
}

/**
 * @description If in body is "count" key, function add and save result into redis
 * @export
 * @param {{ [key: string]: any }} body
 * @param {((err: Error | undefined, result: undefined | number) => void)} cb
 */
export function saveCount(
    body: { [key: string]: any },
    cb: (err: Error | undefined, result: undefined | number) => void
): void {
    if (COUNT in body) {
        if (!Number.isInteger(body[COUNT])) {
            return cb(
                new Error(
                    `Value "${body[COUNT]}" (type "${typeof body[
                        COUNT
                    ]}") is not number`
                ),
                undefined
            );
        }
        addCounter(parseInt(body[COUNT]), cb);
    } else {
        cb(undefined, undefined);
    }
}

/**
 * @description Function add value into redis
 * @export
 * @param {number} value
 * @param {((err: Error | undefined, result: undefined | number) => void)} cb
 * @returns {void}
 */
export function addCounter(
    value: number,
    cb: (err: Error | undefined, result: undefined | number) => void
): void {
    incrementKey(COUNT, value, (err, result) => {
        if (err) {
            return cb(err, undefined);
        }
        if (!result) {
            return cb(new Error('Redis result is empty'), undefined);
        }
        cb(undefined, result[COUNT]);
    });
}

/**
 * @description Function append body into file
 * @export
 * @param {{ [key: string]: any }} body
 * @param {((
 *         err: Error | undefined,
 *         result: undefined | { ts: Date; body: { [key: string]: any } }
 *     ) => void)} cb
 */
export function saveBody(
    body: { [key: string]: any },
    cb: (
        err: Error | undefined,
        result: undefined | { ts: Date; body: { [key: string]: any } }
    ) => void
): void {
    const record = { ts: new Date(), body };
    fs.appendFile('bodies.log', `${JSON.stringify(record)}\n`, (err) => {
        if (err) {
            cb(err, undefined);
        } else {
            cb(undefined, record);
        }
    });
}
