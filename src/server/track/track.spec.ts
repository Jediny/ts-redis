require('dotenv').config();
import * as supertest from 'supertest';
import server from './../index';

jest.mock('../libs/redis-source');
jest.mock('fs');

describe('POST /track', () => {
    it('Restponse code 200 - without count in body', (cb) => {
        supertest(server)
            .post('/track')
            .send({
                test: 'test'
            })
            .set('Accept', 'application/json')
            .expect(200)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                if (!res.body.save) {
                    throw new Error('Key "save" is missing in response body');
                }
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });
    it('Restponse code 200 - with count in body', (cb) => {
        supertest(server)
            .post('/track')
            .send({
                test: 'test',
                count: 5
            })
            .set('Accept', 'application/json')
            .expect(200)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                if (!res.body.save) {
                    throw new Error('Key "save" is missing in response body');
                }
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });
    it('Restponse code 500 - with string count in body', (cb) => {
        supertest(server)
            .post('/track')
            .send({
                test: 'test',
                count: 'test'
            })
            .set('Accept', 'application/json')
            .expect(500)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                if (!res.body.err) {
                    throw new Error('Key "err" is missing in response body');
                }
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });
    it('Restponse code 500 - with string count in body', (cb) => {
        supertest(server)
            .post('/track')
            .send({
                test: 'test',
                count: '5'
            })
            .set('Accept', 'application/json')
            .expect(500)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                if (!res.body.err) {
                    throw new Error('Key "err" is missing in response body');
                }
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });
});
