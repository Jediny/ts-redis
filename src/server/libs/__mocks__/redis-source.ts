const initCountValue = 1;

export function getValueByKey(
    _key: string,
    cb: (err: Error | undefined, value: undefined | string) => void
) {
    cb(undefined, initCountValue.toString());
}

export function incrementKey(
    key: string,
    value: number,
    cb: (
        err: Error | undefined,
        result: undefined | { [key: string]: any }
    ) => void
): void {
    cb(undefined, { [key]: value + initCountValue });
}
