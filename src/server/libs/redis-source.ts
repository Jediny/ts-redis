import * as redis from 'redis';

/**
 * @description Function increases key in Redis by value
 * @export
 * @param {string} key
 * @param {number} value
 * @param {((
 *         err: Error | undefined,
 *         result: undefined | { [key: string]: any }
 *     ) => void)} cb
 */
export function incrementKey(
    key: string,
    value: number,
    cb: (
        err: Error | undefined,
        result: undefined | { [key: string]: any }
    ) => void
): void {
    const client = redis.createClient(getClientConfig());
    client.once('error', (err) => {
        console.error(err);
        return cb(new Error('Cannot connect to DB'), undefined);
    });
    client.incrby(key, value, (err, newValue) => {
        client.quit();
        if (err) {
            cb(err, undefined);
        } else {
            cb(undefined, { key: newValue });
        }
    });
}

/**
 * @description Function returns value by key from redis
 * @export
 * @param {string} key
 * @param {((err: Error | undefined, value: undefined | string) => void)} cb
 */
export function getValueByKey(
    key: string,
    cb: (err: Error | undefined, value: undefined | string) => void
): void {
    const client = redis.createClient(getClientConfig());
    client.on('error', (err) => {
        console.error(err);
        return cb(new Error('Cannot connect to DB'), undefined);
    });
    client.get(key, (err, data) => {
        client.quit();
        cb(err || undefined, data);
    });
}

function getClientConfig(): redis.ClientOpts {
    return {
        host: process.env.REDIS_HOST || undefined,
        port: process.env.REDIS_PORT
            ? parseInt(process.env.REDIS_PORT)
            : undefined
    };
}
