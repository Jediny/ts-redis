import { Router } from 'express';
import getCount from './count';
import postTrack from './track';

const router = Router();

router.post('/track', postTrack);
router.get('/count', getCount);

export default router;
