require('dotenv').config();
import * as supertest from 'supertest';
import server from './../index';

jest.mock('../libs/redis-source');

describe('GET /count', () => {
    it('Restponse code 200', (cb) => {
        supertest(server)
            .get('/count')
            .expect(200)
            .expect((res: any) => {
                if (!res.body) {
                    throw new Error('Key "body" is missing in response');
                }
                if (!res.body.count) {
                    throw new Error('Key "count" is missing in response body');
                }
                if (typeof res.body.count !== 'number') {
                    throw new Error('Key "count" is not number');
                }
            })
            .end(function(err: Error) {
                if (err) {
                    return cb(err);
                } else {
                    cb();
                }
            });
    });
});
