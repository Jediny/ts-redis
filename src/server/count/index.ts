import { Request, Response } from 'express';
import { getValueByKey } from '../libs/redis-source';

export default function getCount(_req: Request, res: Response) {
    getValueByKey('count', (err, count) => {
        if (err) {
            return res.status(500).json({ err: err.message });
        } else {
            return res.json({ count: count ? parseInt(count) : 0 });
        }
    });
}
