# TypeScript Redis
Main project target is create application using TypeScript technology and show basic usage with Redis and file system.

## Dependencies
In directory `redis-docker` is docker compose config file to run Redis server.

``` bash
# run redis server from ./redis-docker directory
$ docker-compose up
```

## Usage
``` bash
# run application in development mode
$ npm run dev

# build application into ./dist directory
$ npm run build

# run application from build package
$ npm run start

# run all tests
$ npm run test
```